/**
 * Cities - Class which contains uniq Cities
 * @class
 */
class Cities {

  /**
   * @type {array}
   */
  static collector = [];


  /**
   * add - add new city to the set
   *
   * @param {string} word city name
   */
  add(word) {
    Cities.collector = [...new Set(Cities.collector.concat(word))].sort();
  }


  /**
   * get - Get city by index
   * @method
   *
   * @throws {Error} if index not found
   *
   * @param {number} index city index
   *
   * @return {string} city name
   */
  getCity(index) {
    if (Cities.collector[index] === undefined) throw new Error('index not found');
    return Cities.collector[index];
  }


  /**
   * get - get Cities
   *
   * @returns {array} - Cities list
   */
  get() {
    return Cities.collector;
  }

}

export default Cities;
